# template

## 项目描述
  纯净版 npm 包编写模板


## 本地开发
### 安装基础依赖
```Basic
npm i / yarn
```
### 单元测试
```Basic
npm run test / yarn test

```
### 打包
```Basic
npm run build / yarn build
```
### 打包后产物目录：./dist